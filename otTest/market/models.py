from otree.api import (
    models,
    widgets,
    BaseConstants,
    BaseSubsession,
    BaseGroup,
    BasePlayer,
    Currency as c,
    currency_range,
)

import random
# from random import seed
# from random import randint

doc = """
This is a one-shot "Prisoner's Dilemma". Two players are asked separately
whether they want to cooperate or defect. Their choices directly determine the
payoffs. 
"""

class Constants(BaseConstants):
    name_in_url = 'market'
    players_per_group = 2
    num_rounds = 3

    instructions_template = 'market/instructions.html'

class Subsession(BaseSubsession):
    def creating_session(self):
        for g in self.get_groups():
            for p in g.get_players():
                p.random_parameter = random.choice([1,2,3,4,5])
                if p.id_in_group == 1:
                    p.bothenter_p1_payoff = p.random_parameter/3
                    p.p1enter_p1_payoff = p.random_parameter
                    p.p1exit_p1_payoff = 0
                    p.bothexit_p1_payoff = 0
                else:
                    p.bothenter_p2_payoff = p.random_parameter/3
                    p.p2exit_p2_payoff = 0
                    p.p2enter_p2_payoff = p.random_parameter
                    p.bothexit_p2_payoff = 0
                # p.payout_parameters(p.random_parameter, p.bothenter_p1_payoff, p.bothexit_p1_payoff, p.p1enter_p1_payoff, p.p1exit_p1_payoff, p.bothenter_p2_payoff, p.p2exit_p2_payoff, p.p2enter_p2_payoff, p.bothexit_p2_payoff)
                
class Group(BaseGroup):
    def set_payoffs(self):
        for p in self.get_players():
            p.set_payoff()

class Player(BasePlayer):
    decision = models.StringField(
        choices=[['Enter', 'Enter'], ['Exit', 'Exit']],
        doc="""This player's decision""",
        widget=widgets.RadioSelect,
    )

    random_parameter = models.DecimalField(max_digits = 3, decimal_places=2)
    bothenter_p1_payoff = models.DecimalField(max_digits = 3, decimal_places=2)
    p1enter_p1_payoff = models.DecimalField(max_digits = 3, decimal_places=2)
    p1exit_p1_payoff = models.DecimalField(max_digits = 3, decimal_places=2)
    bothexit_p1_payoff = models.DecimalField(max_digits = 3, decimal_places=2)
    bothenter_p2_payoff = models.DecimalField(max_digits = 3, decimal_places=2)
    p2exit_p2_payoff = models.DecimalField(max_digits = 3, decimal_places=2)
    p2enter_p2_payoff = models.DecimalField(max_digits = 3, decimal_places=2)
    bothexit_p2_payoff = models.DecimalField(max_digits = 3, decimal_places=2)

    def other_player(self):
        return self.get_others_in_group()[0]

    # def random_parameter(self):
    #     parameter = random.choice([1,2,3,4,5])
    #     return parameter
        
    def set_payoff(self):
        
        if self.id_in_group == 1:
            payoff_matrix = dict(
                Enter=dict(
                    Enter=self.bothenter_p1_payoff,
                    Exit=self.p1enter_p1_payoff,
                ),
                Exit=dict(
                    Enter=self.p1exit_p1_payoff,
                    Exit=self.bothexit_p1_payoff,
                ),
            )
        else:
            payoff_matrix = dict(
                Enter=dict(
                    Enter=self.bothenter_p2_payoff,
                    Exit=self.p2enter_p2_payoff,
                ),
                Exit=dict(
                    Enter=self.p2exit_p2_payoff, 
                    Exit=self.bothexit_p2_payoff
                ),
            )

        self.payoff = payoff_matrix[self.decision][self.other_player().decision]

        # print(self.id_in_group)
        # for x in payoff_matrix:
        #     print (x)
        #     for y in payoff_matrix[x]:
        #         print (y,':',payoff_matrix[x][y])
        

